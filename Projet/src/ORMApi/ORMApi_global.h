#ifndef ORMAPI_GLOBAL_H
#define ORMAPI_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ORMAPI_LIBRARY)
#  define ORMAPI_EXPORT Q_DECL_EXPORT
#else
#  define ORMAPI_EXPORT Q_DECL_IMPORT
#endif

#endif // ORMAPI_GLOBAL_H
