/********************************************************************************
** Form generated from reading UI file 'dialog_solveur.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_SOLVEUR_H
#define UI_DIALOG_SOLVEUR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog_solveur
{
public:
    QLabel *label;
    QPlainTextEdit *plainTextEdit;
    QPushButton *pushButton;

    void setupUi(QDialog *Dialog_solveur)
    {
        if (Dialog_solveur->objectName().isEmpty())
            Dialog_solveur->setObjectName(QString::fromUtf8("Dialog_solveur"));
        Dialog_solveur->resize(400, 300);
        label = new QLabel(Dialog_solveur);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 10, 371, 101));
        plainTextEdit = new QPlainTextEdit(Dialog_solveur);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(20, 120, 371, 141));
        pushButton = new QPushButton(Dialog_solveur);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(160, 270, 80, 21));

        retranslateUi(Dialog_solveur);

        QMetaObject::connectSlotsByName(Dialog_solveur);
    } // setupUi

    void retranslateUi(QDialog *Dialog_solveur)
    {
        Dialog_solveur->setWindowTitle(QCoreApplication::translate("Dialog_solveur", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("Dialog_solveur", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Pour utiliser le solveur : marquez tous le sommets du labyrinthe dans l'ordre </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">croissant en commen\303\247ant par 0. En premi\303\250re ligne, mettez l'identifiant </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">du sommet d'arriv\303\251 Exemple :</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-inde"
                        "nt:0; text-indent:0px;\">3</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">0 1</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1 2</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1 3 etc</p></body></html>", nullptr));
        pushButton->setText(QCoreApplication::translate("Dialog_solveur", "Solve", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog_solveur: public Ui_Dialog_solveur {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_SOLVEUR_H
