#ifndef DEFS_H
#define DEFS_H

#define GRID_SIZE 100                   /* Taille de la grille (nombre de boutons) */
#define LINE_LENGTH 10                  /* Taille d'une ligne dans la grille */
#define MIN_INDEX_LINE 0                /* Index min d'une ligne dans la grille */
#define MAX_INDEX_LINE 9                /* Index max d'une ligne dans la grille */
#define BTN_WIDTH 50                    /* Largeur d'un bouton */
#define BTN_HEIGHT 50                   /* Hauteur d'un bouton */

#define NO_ICON ""                          /* Pas d'image */
#define MUR_PNG "../img/mur.png"            /* Image du mur */
#define ROBOT_PNG "../img/robot.png"        /* Image du robot */
#define ARRIVEE_PNG "../img/arrivee.png"    /* Image de l'arrivée */
#define GAGNE_PNG "../img/gagne.png"        /* Image quand le robot arrive sur l'arrivée => gagné */
#define ROBOT_MORT_PNG "../img/robot_mort.png"  /* Image du robot quand il meurt */
#define PIEGE_PNG "../img/piege.png"            /* Image d'un piège */
#define BOUE_PNG "../img/boue.png"          /* Image de la boue */

#define RIEN 0      /* Identifiant quand il n'y a rien */
#define MUR 1       /* Identifiant pour un mur */
#define ROBOT 2     /* Identifiant pour le robot */
#define ARRIVEE 3   /* Identifiant de l'arrivée */
#define PIEGE 4     /* Identifiant d'un piège */
#define BOUE 5      /* Identificant de la boue */

#define LVL_EXT ".lvl"  /* Extension de fichier niveau */

#define TTY_CONSOLE 1       /* Permet de savoir si le programme est lancé depuis la console */
#define TTY_NOT_CONSOLE 2   /* Permet de savoir si le programme est lancé en mode GUI */

#define ORM_LIB L"./lib/QxOrm.dll"

#define CALC_BEST_SCORE(x) ((x + 1) / 2) /* On fait une division par deux car les event keyboards vont par deux aussi (deux états : pressed, released) */

/* Définition de types à taille unique */
typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef signed short int int16_t;
typedef unsigned short int uint16_t;
typedef signed int int32_t;

/* Énumération des identifiants des messages de log */
enum LOGMSG_ID_INFO
{
    LOGMSG_START_APP,           /*Lancement de l'application Robot Ricochet. */
    LOGMSG_CREATION_FENETRE,    /*Creation de la fenetre. */
    LOGMSG_OPEN_LEVEL_MAKER,    /*Ouverture de la fenetre level maker. */
    LOGMSG_OPEN_PLAY_WINDOW,    /*Ouverture de la fenetre de jeu. */
    LOGMSG_TRY_TO_CONNECT,      /*Tentative de connexion... */
    LOGMSG_CONNECTION_SUCCEED,  /*Connexion établie. */
    LOGMSG_CANT_CONNECT,        /*Impossible de se connecter avec l'identifiant : [%s] et le mot de passe : [%s]. */
    LOGMSG_CLOSE_APP,           /*Fermeture de l'applicaton. */
    LOGMSG_EMPTY_CREATOR_NAME,  /*Nom du createur de niveau non definit. */
    LOGMSG_SET_CREATOR_NAME,    /*Nom du createur de niveau : [%s]. */
    LOGMSG_BEGIN_WRITE_FILE,    /*Debut de l'ecriture du fichier [%s]. */
    LOGMSG_STOP_WRITE_FILE,     /*Fin de l'ecriture du fichier [%s]. */
    LOGMSG_SAVE_LVL_IN_DB,      /*Sauvegarde du niveau dans la base de donnees. */
    LOGMSG_ERASE_LEVEL_MAKER,   /*Remise a zero de la fenetre level maker. */
    LOGMSG_OUTIL_PIEGE_CHOISI,  /*Outil piege selectionne. */
    LOGMSG_OUTIL_BOUE_CHOISI,   /*Outil boue selectionne. */
    LOGMSG_OUTIL_ROBOT_CHOISI,  /*Outil robot selectionne. */
    LOGMSG_OUTIL_MUR_CHOISI,    /*Outil mur selectionne. */
    LOGMSG_OUTIL_NONE_CHOISI,   /*Outil vide selectionne. */
    LOGMSG_OUTIL_ARRIVEE_CHOISI,/*Outil arrivee selectionne. */
    LOGMSG_IMAGE_PLACE,         /*Image : [%s] place a la position [%d]. */
    LOGMSG_PLAYER_SCORE,        /*Joueur : [%s] a terminé le niveau en %d coups. */
    LOGMSG_LOAD_LEVEL_SUCCEED,  /*Le niveau a correctement été chargé. */

    NB_MAX_LOG_MSG_INFO,
};

/* Définitions des codes d'erreur (servent aussi d'identifiant de message de log) */
enum err_codes
{
    FILE_NOT_OPENED = NB_MAX_LOG_MSG_INFO,  /* Fichier non ouvert (ou non choisi) */
    MEMORY_ALLOCATION_ERR,  /* Erreur d'allocation mémoire */
    READ_FILE_ERROR,        /* Erreur de lecture du fichier */
    EMPTY_CREATOR_NAME,     /* Nom du créateur du niveau non défini */
    OUT_OF_BOUND,           /* Sortie des limites du tableau */
    UNSOLVABLE,             /* Le niveau n'est pas solvable */

    NB_MAX_ERROR,
};

/* Tableau des messages de log */
static const char* tabLogMsg[]
{
    /**************************/
    /* Message de niveau INFO */
    /**************************/

/* LOGMSG_START_APP             */ "Lancement de l'application Robot Ricochet.\n",
/* LOGMSG_CREATION_FENETRE      */ "Creation de la fenetre.\n",
/* LOGMSG_OPEN_LEVEL_MAKER      */ "Ouverture de la fenetre level maker.\n",
/* LOGMSG_OPEN_PLAY_WINDOW      */ "Ouverture de la fenetre de jeu.\n",
/* LOGMSG_TRY_TO_CONNECT        */ "Tentative de connexion...\n",
/* LOGMSG_CONNECTION_SUCCEED    */ "Connexion établie.\n",
/* LOGMSG_CANT_CONNECT          */ "Impossible de se connecter avec l'identifiant : [%s] et le mot de passe : [%s].\n",
/* LOGMSG_CLOSE_APP             */ "Fermeture de l'applicaton.\n",
/* LOGMSG_EMPTY_CREATOR_NAME    */ "Nom du createur de niveau non definit.\n",
/* LOGMSG_SET_CREATOR_NAME      */ "Nom du createur de niveau : [%s].\n",
/* LOGMSG_BEGIN_WRITE_FILE      */ "Debut de l'ecriture du fichier [%s].\n",
/* LOGMSG_STOP_WRITE_FILE       */ "Fin de l'ecriture du fichier [%s].\n",
/* LOGMSG_SAVE_LVL_IN_DB        */ "Sauvegarde du niveau dans la base de donnees.\n",
/* LOGMSG_ERASE_LEVEL_MAKER     */ "Remise a zero de la fenetre level maker.\n",
/* LOGMSG_OUTIL_PIEGE_CHOISI    */ "Outil piege selectionne.\n",
/* LOGMSG_OUTIL_BOUE_CHOISI     */ "Outil boue selectionne.\n",
/* LOGMSG_OUTIL_ROBOT_CHOISI    */ "Outil robot selectionne.\n",
/* LOGMSG_OUTIL_MUR_CHOISI      */ "Outil mur selectionne.\n",
/* LOGMSG_OUTIL_NONE_CHOISI     */ "Outil vide selectionne.\n",
/* LOGMSG_OUTIL_ARRIVEE_CHOISI  */ "Outil arrivee selectionne.\n",
/* LOGMSG_IMAGE_PLACE           */ "Image : [%s] place a la position [%d].\n",
/* LOGMSG_PLAYER_SCORE          */ "Joueur : [%s] a terminé le niveau en %d coups.\n",
/* LOGMSG_LOAD_LEVEL_SUCCEED    */ "Le niveau a correctement été chargé.\n",

    /***************************/
    /* Message de niveau ERROR */
    /***************************/

/* FILE_NOT_OPENED              */ "Le fichier [%s] n'a pas pu être ouvert.\n",
/* MEMORY_ALLOCATION_ERR        */ "Impossible d'allouer de la mémoire.\n",
/* READ_FILE_ERROR              */ "Impossible de lire le fichier [%s].\n",
/* EMPTY_CREATOR_NAME           */ "Le nom du créateur de niveau n'est pas renseigné.\n",
/* OUT_OF_BOUND                 */ "Erreur interne : Sortie des limites du tableau.\n",
/* UNSOVABLE                    */ "Le niveau n'est pas solvable.\n",
};

#endif // DEFS_H
