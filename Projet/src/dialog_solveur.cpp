#include "dialog_solveur.h"
#include "ui_dialog_solveur.h"

Dialog_solveur::Dialog_solveur(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_solveur)
{
    ui->setupUi(this);
}

Dialog_solveur::~Dialog_solveur()
{
    delete ui;
}

void Dialog_solveur::getSolvCoords(QString & str)
{
    str = ui->plainTextEdit->toPlainText();
}
