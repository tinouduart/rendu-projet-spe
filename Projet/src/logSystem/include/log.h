#ifndef LOG_INCLUDED_H
#define LOG_INCUDED_H

/* Permet de définir un exporteur commun à linux et windows (dans l'absolue, on s'en fiche pour linux, mais ça permet d'avoir du code cohérent) */
#ifdef _WIN32
#define LOG_API __declspec(dllexport)
#else
#define LOG_API
#endif

/* Si la bibliothèque doit être utilisée avec du C++ on est obligé de déclarer un extern "C" */
#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <time.h>

#define LOG_SYS_FILE 1
#define LOG_SYS_STDOUT 2

#define LOG_LEVEL_ERROR 4
#define LOG_LEVEL_WARNING 3
#define LOG_LEVEL_DEBUG 2
#define LOG_LEVEL_INFO 1

#define TRUE 1
#define FALSE 0

/* int logger(int logSys, const char* functionName, int logLevel, const char* message, ...);
 * Fonction de journalisation permettant d'écrire le fonctionnement du programme.
 * 
 * int logSys : LOG_SYS_FILE | LOG_SYS_STDOUT, permet de définir où seront écrit les logs.
 * const char* functionName : Nom de la fonction appelante.
 * int logLevel : Niveau de log (LOG_LEVEL_ERROR | LOG_LEVEL_WARNING | LOG_LEVEL_INFO)
 * const char* message : message de log.
 */
LOG_API int logger(int logSys, const char* functionName, int logLevel, const char* message, ...);

/* void freeLog();
 * Permet de libérer la mémoire allouée pour le système de logging.
 */
LOG_API void freeLog();

/* Pas besoin ici */
int formatAndLog(int logSys, const char* logMsg, const char* functionName, int logLevel);
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif