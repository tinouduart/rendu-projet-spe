#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtWidgets>
#include <QFileDialog>
#include <QMessageBox>

#include <iostream>
#include <fstream>

#ifdef __unix__
#include <QxOrm.h>
#include "export.h"
#include <QxOrm_Impl.h>
#include "ormapi.h"
#endif /* __unix__ */

#include "solveur.h"

#include "log.h"

#include "dialog_solveur.h"

#ifdef __unix__
#include <unistd.h>
#endif /* __unix__ */

#ifdef _WIN32
#include <io.h>
#define STDOUT_FILENO 1
#endif /* _WIN32 */

static QVector<QPushButton*> buttonList(GRID_SIZE); /* Pareil qu'en haut => ça permettra de modifier les images des boutons */
static int selected = RIEN; /* Variable globale pour savoir quel outil a été sélectionné */
static niveau lvl("");
static int isConsole = 0;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_CREATION_FENETRE]);
    const QSize size(BTN_WIDTH, BTN_HEIGHT);

    /* On cache le label qui indique si le niveau a été chargé ou non au lancement de la fenêtre */
    ui->statusLabel->hide();

    /* On met les images dans les boutons 'outils' */
    ui->outilMur->setIcon(QIcon(MUR_PNG));
    ui->outilRobot->setIcon(QIcon(ROBOT_PNG));
    ui->outilArrivee->setIcon(QIcon(ARRIVEE_PNG));
    ui->outilPiege->setIcon(QIcon(PIEGE_PNG));
    ui->outilBoue->setIcon(QIcon(BOUE_PNG));

    /* On crée un signalMapper pour pouvoir passer un paramètre au callback boutonclic */
    QSignalMapper* signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(appliquerImage(int)));

    /* on veut 100 boutons */
    for(int i = 0; i < GRID_SIZE; i++)
    {
        /* création d'un bouton à chaque tour de boucle */
        buttonList[i] = new QPushButton("", this);

        /* On lui donne une taille fixe */
        buttonList[i]->setMinimumSize(BTN_WIDTH, BTN_HEIGHT);
        buttonList[i]->setIconSize(size);

        /* On le place dans le gridLayout à la colonne i/10 et à la ligne i%10 */
        ui->gridLayout->addWidget(buttonList.at(i), i / 10, i % 10);

        /* On connecte le bouton à son callback */
        signalMapper->setMapping(buttonList[i], i);
        connect(buttonList[i], SIGNAL(clicked()), signalMapper, SLOT(map()));
    }
}

/* void MainWindow::appliquerImage(int pos);
 * Permet de mettre une image dans un bouton (en d'autres termes, permet de modifier la grille de jeu)
 *
 * int pos : position du bouton dans le tableau
 */
void MainWindow::appliquerImage(int pos)
{
    /* on crée le QIcon que l'on va mettre dans notre bouton */
    QPixmap pixmap;
    QIcon icon;

    std::string outil = "outil ";

    /* on lui donne la même taille que celle du bouton => fill */
    QSize iconSize = QSize(BTN_WIDTH, BTN_HEIGHT);

    if (selected == MUR)
    {
        pixmap.load(MUR_PNG);
        outil += "mur";
    }

    if (selected == ROBOT)
    {
        pixmap.load(ROBOT_PNG);
        iconSize = QSize(BTN_WIDTH, BTN_HEIGHT);
        outil += "robot";
    }

    if (selected == ARRIVEE)
    {
        pixmap.load(ARRIVEE_PNG);
        iconSize = QSize(BTN_WIDTH, BTN_HEIGHT);
        outil += "arrivee";
    }

    if (selected == PIEGE)
    {
        pixmap.load(PIEGE_PNG);
        iconSize = QSize(BTN_WIDTH, BTN_HEIGHT);
        outil += "piege";
    }

    if (selected == BOUE)
    {
        pixmap.load(BOUE_PNG);
        iconSize = QSize(BTN_WIDTH, BTN_HEIGHT);
        outil += "boue";
    }

    if (selected == RIEN)
    {
        pixmap.load(NO_ICON);
        outil += "vide";
    }

    icon.addPixmap(pixmap);

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_IMAGE_PLACE], outil.c_str(), pos);
    /* on place l'image dans le bouton puis on refixe la taille du bouton */
    buttonList[pos]->setIcon(icon);
    buttonList[pos]->setIconSize(iconSize);
    buttonList[pos]->setFixedSize(BTN_WIDTH, BTN_HEIGHT);

    /* on met à jour le tableau d'entier */
    lvl.creerTableau(pos, selected);
}

MainWindow::~MainWindow()
{
    /* Libération de la mémoire pour tous les boutons */
    for (int i = 0; i < GRID_SIZE; i++)
        delete buttonList[i];

    delete ui;
}

/* int MainWindow::on_Exporter_clicked();
 * Permet d'exporter la grille de jeu actuelle dans un fichier (plus tard dans une bdd)
 *
 * Renvoie 0 si tout s'est bien passé, un code d'erreur (< 0) sinon.
 */
int MainWindow::on_Exporter_clicked()
{
    /* On récupère le contenu du lineEdit */
    QString str = ui->creatorLineEdit->text();

    /* Initialisation d'un message d'erreur (au cas où) */
    QString err = "";

    std::string obstaclesString = "";

    /* Si la lineEdit est vide ou ne contient que des espaces => erreur */
    if (str.trimmed().isEmpty())
    {
        /* On crée le message d'erreur */
        err.append(QString("Error ")).append(QString::number(EMPTY_CREATOR_NAME)).append(" : You must enter a valid name.");

        /* On l'affiche dans une messageBox */
        QMessageBox::critical(this, tr("Error"), err, QMessageBox::Cancel);

        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, tabLogMsg[LOGMSG_EMPTY_CREATOR_NAME]);
        /* Et on renvoie le code d'erreur */
        return EMPTY_CREATOR_NAME;
    }
    lvl.setCreator(str.toStdString());
    std::ofstream file;

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_SET_CREATOR_NAME], str.toStdString().c_str());

    QString filename = QFileDialog::getSaveFileName(this, tr("Save Level"), ".", tr("Level Files (*.lvl)"));

    /* On force l'extension du fichier si jamais l'utilisateur ne la précise pas */
    if (!filename.endsWith(LVL_EXT))
        filename += LVL_EXT;

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_BEGIN_WRITE_FILE], filename.toStdString().c_str());
    file.open(filename.toStdString());
    file << str.toStdString() << "\n";
    file << lvl.getDate() << "\n";
    for (int i = 0; i < GRID_SIZE; i++)
    {
        if (lvl.obstaclesArray[i] == ROBOT)
            lvl.setRobotPosition(i); /* On set la position du robot */

        file << lvl.obstaclesArray[i] << " "; /* On écrit le fichier local */

        /* Création de la chaine niveau */
        obstaclesString += std::to_string(lvl.obstaclesArray[i]) + " ";
    }
    file.close();
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_STOP_WRITE_FILE], filename.toStdString().c_str());

    lvl.obstaclesString = obstaclesString;

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_SAVE_LVL_IN_DB]);

#ifdef __unix__
    /* On effectue la sauvegarde en base */
    typedef std::shared_ptr<niveau> niveau_ptr;
    niveau_ptr lvlToStore; lvlToStore.reset(new niveau(lvl));

    typedef std::vector<niveau_ptr> type_lst_niveau;
    type_lst_niveau lst_niveau;
    lst_niveau.push_back(lvlToStore);

    ORMApi* db = new ORMApi((char*)"QSQLITE", (char*)"test_qxorm.db", (char*)"localhost", (char*)"root", (char*)"");
    db->createDatabase();

    QSqlError daoError = db->create_table<niveau>();
    daoError = qx::dao::insert(lst_niveau);

    /* Libération mémoire du pointeur db */
    delete db;
#endif /* __unix__ */
    return 0;
}

/*! int on_Charger_clicked()
 * Permet de charger un niveau depuis un fichier (plus tard depuis une bdd)
 *
 * Renvoie 0 si tout s'est bien passé, un code d'erreur (< 0) sinon
 */
int MainWindow::on_Charger_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Load Level"), ".", tr("Level Files (*.lvl)"));

    QPixmap mur(MUR_PNG);
    QIcon icon_mur(mur);

    QPixmap robot(ROBOT_PNG);
    QIcon icon_robot(robot);

    QPixmap arrivee(ARRIVEE_PNG);
    QIcon icon_arrivee(arrivee);

    QPixmap piege(PIEGE_PNG);
    QIcon icon_piege(piege);

    QPixmap boue(BOUE_PNG);
    QIcon icon_boue(boue);

    /* Initialisation d'un message d'erreur (au cas où) */
    QString err = "";

    /* char* (chaine de caractère au format du langage C : contiendra le nom du fichier à charger) */
    char* c_file_name = nullptr;

    /* Contiendra la longueur de la chaine de caractère : mieux vaut ne la calculer qu'une fois plutôt que faire plusieurs appel à la méthode size() */
    int32_t filename_length = 0;

    /* return code */
    int rc = 0;

    /* On récupère la taille de la chaine filename */
    filename_length = (int32_t)filename.toStdString().size();

    /* Allocation mémoire pour la chaine */
    c_file_name = reinterpret_cast<char*>(malloc((filename_length + 1) * sizeof(char)));
    if ( !c_file_name )
    {
        /* On crée le message d'erreur */
        err.append(QString("Error ")).append(QString::number(MEMORY_ALLOCATION_ERR)).append(" : Not enough memory.");

        /* On l'affiche dans une messageBox */
        QMessageBox::critical(this, tr("Error"), err, QMessageBox::Cancel);
        return MEMORY_ALLOCATION_ERR;
    }

    /* Copie de la chaine filename format C++ vers un char* (format C) */
    filename.toStdString().copy(c_file_name, filename_length + 1);

    /* On place le caractère null `\0` à la fin pour indiquer la fin de la chaine */
    c_file_name[filename.toStdString().size()] = '\0';

    /* Création d'un objet niveau pour pouvoir charger un niveau en mémoire */
    lvl = niveau("");
    rc = lvl.chargerNiveau(c_file_name);

    /* Gestion des erreurs */
    switch (rc)
    {
        case 0:
            break;
        case FILE_NOT_OPENED:
            /* On crée le message d'erreur */
            err.append(QString("Error ")).append(QString::number(FILE_NOT_OPENED)).append(" : Cannot open file.");

            /* On l'affiche dans une messageBox */
            QMessageBox::critical(this, tr("Error"), err, QMessageBox::Cancel);
            return FILE_NOT_OPENED;
        case READ_FILE_ERROR:
            /* On crée le message d'erreur */
            err.append(QString("Error ")).append(QString::number(READ_FILE_ERROR)).append(" : Cannot read file.");

            /* On l'affiche dans une messageBox */
            QMessageBox::critical(this, tr("Error"), err, QMessageBox::Cancel);
            return READ_FILE_ERROR;
    }

    /* Parcourt du tableau d'obstacles */
    for (int i = 0; i < GRID_SIZE; i++)
    {
        /* En fonction du tableau, on met à jour l'interface */
        switch (lvl.obstaclesArray[i])
        {
            case RIEN:
                buttonList[i]->setIcon(QIcon());
                break;
            case MUR:
                buttonList[i]->setIcon(icon_mur);
                break;
            case ROBOT:
                buttonList[i]->setIcon(icon_robot);
                break;
            case ARRIVEE:
                buttonList[i]->setIcon(icon_arrivee);
                break;
            case PIEGE:
                buttonList[i]->setIcon(icon_piege);
                break;
            case BOUE:
                buttonList[i]->setIcon(icon_boue);
                break;
            default:
                break;
        }
    }

    /* On libère la mémoire allouée */
    free(c_file_name);

    /* Tout s'est bien passé, on renvoie 0 */
    return 0;
}

void MainWindow::on_outilRobot_clicked()
{
    selected = ROBOT;
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OUTIL_ROBOT_CHOISI]);
}

void MainWindow::on_outilMur_clicked()
{
    selected = MUR;
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OUTIL_MUR_CHOISI]);
}

void MainWindow::on_outilNone_clicked()
{
    selected = RIEN;
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OUTIL_NONE_CHOISI]);
}

void MainWindow::on_outilArrivee_clicked()
{
    selected = ARRIVEE;
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OUTIL_ARRIVEE_CHOISI]);
}

void MainWindow::on_clearButton_clicked()
{
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_ERASE_LEVEL_MAKER]);
    /* On parcourt les deux tableaux (celui des boutons et le tableau des obstacles) */
    for (int i = 0; i < GRID_SIZE; i++)
    {
        /* On réinitialise chaque case */
        buttonList[i]->setIcon(QIcon());
    }

    /* Réinitialisation du niveau */
    lvl.reset();
}

void MainWindow::on_outilPiege_clicked()
{
    selected = PIEGE;
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OUTIL_PIEGE_CHOISI]);
}

void MainWindow::on_outilBoue_clicked()
{
    selected = BOUE;
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OUTIL_BOUE_CHOISI]);
}

void MainWindow::on_pushButton_clicked()
{
    QString coordsSommets;
    solveur* s = nullptr;

    int16_t rc = 0;
    Dialog_solveur* dialog = new Dialog_solveur();
    int16_t nbSommets = 0;
    int arrivee = 0;
    int x, y = 0;

    FILE* f = nullptr;

    if (dialog->exec() == 0)
        dialog->getSolvCoords(coordsSommets);

    for (int i = 0; i < coordsSommets.length(); i++)
    {
        if (coordsSommets.toStdString().c_str()[i] == '\n')
            nbSommets++;
    }
    nbSommets++;
    s = new solveur(nbSommets);

    f = fopen("tmp", "w");
    fprintf(f, "%s", coordsSommets.toStdString().c_str());
    fclose(f);

    f = fopen("tmp", "r");
    fscanf(f, "%d\n", &arrivee);
    while (!feof(f))
    {
        fscanf(f, "%d %d\n", &x, &y);
        s->addEdge(x, y);
    }
    fclose(f);
    remove("tmp");

    rc = s->dijkstra(s->adjMatrix, 0, arrivee);
    if (!rc)
    {
        QMessageBox::critical(this, tr("Error"), tr("Niveau non solvable"), QMessageBox::Cancel);
        return;
    }
    QMessageBox::information(this, tr("Success"), tr("Le niveau est solvable"), QMessageBox::Ok);
    return;
}
