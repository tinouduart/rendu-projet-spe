#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

#include "niveau.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void appliquerImage(int pos);

private slots:

    /* int on_Exporter_clicked();
     * Permet d'exporter la grille de jeu actuelle dans un fichier (plus tard dans une bdd)
     *
     * Renvoie 0 si tout s'est bien passé, un code d'erreur (< 0) sinon.
     */
    int on_Exporter_clicked();

    /* int on_Charger_clicked();
     * Permet de charger un niveau.
     *
     * Renvoie 0 si tout s'est bien passé, un code d'erreur (<0) sinon.
     */
    int on_Charger_clicked();

    /* void on_outilRobot_clicked();
     * Permet de sélectionner l'outil 'robot'.
     */
    void on_outilRobot_clicked();

    /* void on_outilRobot_clicked();
     * Permet de sélectionner l'outil 'mur'.
     */
    void on_outilMur_clicked();

    /* void on_outilRobot_clicked();
     * Permet de sélectionner l'outil 'rien'.
     */
    void on_outilNone_clicked();

    /* void on_outilRobot_clicked();
     * Permet de sélectionner l'outil 'arrivée'.
     */
    void on_outilArrivee_clicked();

    /* void on_outilRobot_clicked();
     * Permet d'effacer la grille.
     */
    void on_clearButton_clicked();

    /* void on_outilRobot_clicked();
     * Permet de sélectionner l'outil 'piège'.
     */
    void on_outilPiege_clicked();

    /* void on_outilRobot_clicked();
     * Permet de sélectionner l'outil 'boue'.
     */
    void on_outilBoue_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
