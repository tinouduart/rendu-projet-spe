#include "menu.h"
#include "ui_menu.h"
#include "player.h"
#include "externVars.h" /* Pour la variable extern player qui sera utilisée un peu partout lors d'une exécution du programme */

#ifdef __unix__
#include "ormapi.h"
#endif /* __unix__ */

#include "log.h"

#include <QApplication>

player* player1 = nullptr;

menu::menu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::menu)
{
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_CREATION_FENETRE]);
    ui->setupUi(this);
}

menu::~menu()
{
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_CLOSE_APP]);
    freeLog();
    delete ui;
}
/* void menu::on_levelMaker_clicked();
 * Permet d'ouvrir la fenêtre d'édition de niveau.
 */
void menu::on_levelMaker_clicked()
{
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OPEN_LEVEL_MAKER]);
    window = new MainWindow(this);
    window->show();
    //this->hide();
}

/* void menu::on_playButton_clicked();
 * Permet d'ouvrir la fenêtre de jeu.
 */
void menu::on_playButton_clicked()
{
    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_OPEN_PLAY_WINDOW]);
    play = new playwindow(this);
    play->show();
    this->hide();
    //this->setVisible(false);
}

void menu::on_connection_clicked()
{
    /* Vérification de l'identifiant et du mot de passe */
#ifdef __unix__
    /* Pointeur sur l'API de communication avec l'ORM */
    ORMApi* db = NULL;

    /* Définitions de type pour faciliter la lecture du code */
    typedef std::shared_ptr<player> player_ptr;
    typedef std::vector<player_ptr> type_lst_player;

    QSqlError daoError;
#endif /* __unix__ */
    /* Ouverture du fichier users dont la structure est celle ci-dessous:
        user1:pass1
        user2:pass2
            ...
        usern:passn
    */
    FILE* userFile = fopen("users", "r");

    /* Allocation dynamique d'un buffer de taille 128 */
    char* buffer = (char*)malloc(128 * sizeof(char));

    /* Variables pour l'utilisation de strtok */
    char* tok = NULL;
    char delim[2] = ":";

    int32_t len = 0;
    int32_t inputLen = 0;
    bool userExists = false;
    bool goodPassword = false;

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_TRY_TO_CONNECT]);

    /* Si l'ouverture du fichier rate ou si le buffer ne peut pas être alloué, on s'arrête */
    if (!userFile || !buffer)
        return;

    /* On initialise le buffer */
    memset(buffer, '\0', 128);

    /* On commence la lecture */
    while (!feof(userFile))
    {
        /* Lecture d'une ligne */
        if (fgets(buffer, 128, userFile) == NULL)
            break;

        /* On récupère l'utilisateur */
        tok = strtok(buffer, delim);
        len = (int32_t)strlen(tok);
        inputLen = (int32_t)strlen(ui->username->text().toStdString().c_str());

        /* Si les tailles de l'input et de l'utilisateur trouvé dans le fichiers sont égales, c'est qu'on a potentiellement trouvé le bon user */
        if (len == inputLen)
        {
            /* On compare les chaines */
            if (!memcmp(tok, ui->username->text().toStdString().c_str(), len))
            {
                /* Si on arrive ici c'est que l'on a trouvé un utilisateur existant */
                userExists = true;
                break;
            }
        }
    }

    if (userExists) /* Le nom d'utilisateur existe => on check le mdp */
    {
        /* Pas besoin de refaire une lecture vu qu'on a trouvé la ligne contenant l'utilisateur */
        /* On utilise strtok sur le pointeur actuel */
        tok = strtok(NULL, delim);
        len = (int32_t)strlen(tok);

        /* On vérifie le caractère de fin d'enregistrement => à adapter sous windows (CRLF) sous MAC (CR) ou juste un nullbyte (\0) */
        if (tok[len - 1] == '\n')
            len -= 1;

        inputLen = (int32_t)strlen(ui->password->text().toStdString().c_str());

        /* On compare les tailles */
        if (len == inputLen)
        {
            /* On compare les chaines */
            if (!memcmp(tok, ui->password->text().toStdString().c_str(), len))
            {
                /* Le mdp existe */
                goodPassword = true;
            }
        }
    }

    /* Si les informations passées en entrée sont valides, on peut modifier notre affichage */
    if (userExists && goodPassword)
    {
        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_CONNECTION_SUCCEED]);
        ui->username->setVisible(false);
        ui->password->setVisible(false);
        ui->connection->setVisible(false);
        ui->playButton->setVisible(true);
        ui->levelMaker->setVisible(true);

        /* On ferme le fichier et on libère la mémoire allouée au buffer */
        fclose(userFile);
        free(buffer);

        /* Création du joueur */
        player* p1 = new player(ui->username->text().toStdString(), ui->password->text().toStdString());
#ifdef __unix__
        /* On effectue la sauvegarde de l'utilisateur en base */
        player_ptr playerToStore; playerToStore.reset(p1);

        type_lst_player lst_player;
        lst_player.push_back(playerToStore);

        db = new ORMApi((char*)"QSQLITE", (char*)"test_qxorm.db", (char*)"localhost", (char*)"root", (char*)"");
        db->createDatabase();

        daoError = db->create_table<player>();
        daoError = qx::dao::insert(lst_player);
#endif /* __unix__ */

        player1 = new player(p1);

    }
    else
    {
        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, tabLogMsg[LOGMSG_CANT_CONNECT], ui->username->text().toStdString().c_str(), ui->password->text().toStdString().c_str());
    }
    /* Libération mémoire du pointeur db */
    //delete db;
}
