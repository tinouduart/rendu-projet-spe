#include "niveau.h"
#include "log.h"
#include <string.h>

#ifdef _WIN32
#include <Windows.h>
#endif /* _WIN32 */

#ifdef __unix__
#include <QxOrm.h>
#include <QxOrm_Impl.h>    // Automatic memory leak detection and boost serialization export macro

QX_REGISTER_CPP_MY_TEST_EXE(niveau)   // This macro is necessary to register 'drug' class in QxOrm context

namespace qx {
template <> void register_class(QxClass<niveau> & t)
{
    /* Permet de définir le nom des colonnes dans la base de données */
    t.id(& niveau::id, "id");
    t.data(& niveau::robotPosition, "robotPosition", 1);
    t.data(& niveau::date, "date");
    t.data(& niveau::creator, "creator");
    t.data(& niveau::obstaclesString, "obstacles");
}}
#endif /* __unix__ */

niveau::niveau(niveau* lvl)
{
    this->robotPosition = lvl->robotPosition;
    this->arriveePosition = lvl->arriveePosition;
    this->creator = lvl->creator;
    this->date = lvl->date;

    for (int i = 0; i < GRID_SIZE; i++)
        this->obstaclesArray[i] = lvl->obstaclesArray[i];
}

niveau::niveau(std::string creator_name)
{
    time_t date = time(nullptr);
    struct tm *atime = localtime(&date);

    int day = atime->tm_mday;
    int month = atime->tm_mon + 1;
    int year = atime->tm_year + 1900;

    this->robotPosition = 0;
    this->arriveePosition = 0;

    for (int i = 0; i < GRID_SIZE; i++)
        this->obstaclesArray[i] = 0;
    this->creator = creator_name;
    this->date = std::to_string(day) + "/" + std::to_string(month) + "/" + std::to_string(year);
}

int niveau::chargerNiveau(char* filename)
{
    /* Une ligne dans un fichier fera toujours 200 caractères donc on met 201 caractères ici pour le '\0' */
    /* Il faudra sûrement adapter ce tableau de manière dynamique en fonction de si le fichier à charger a été créé sous windows ou sous un système unix
        => présence des caractères CRLF ou juste LF */
    char ligne[201] = { '\0' };

    /* Pour strtok */
    char* arrayToken = nullptr;
    char delimiter[2] = " ";

    int res = 0;
    int i = 0;

    FILE* file = nullptr;

    /* On ouvre le fichier */
    file = fopen(filename, "r");

    /* Vérification d'usage : test d'ouverture du fichier */
    if ( !file )
    {
        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, tabLogMsg[FILE_NOT_OPENED], filename);

        /* on libère la mémoire allouée */
        free(filename);
        return FILE_NOT_OPENED; /* pas ouvert ? -> on s'arrête */
    }

    /* 1ere ligne => creator */
    /* Est-ce qu'on peut bien lire une ligne ? */
    if ( !fgets(ligne, 200, file) )
    {
        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, tabLogMsg[READ_FILE_ERROR], filename);
        free(filename);
        return READ_FILE_ERROR; /* non ? -> on s'arrête */
    }
    this->creator = ligne;

    /* On enlève le '\n' */
    this->creator.erase(this->creator.length() - 1);

    /* 2eme ligne => date */
    /* Est-ce qu'on peut bien lire une ligne ? */
    if ( !fgets(ligne, 200, file) )
    {
        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, tabLogMsg[READ_FILE_ERROR], filename);
        free(filename);
        return READ_FILE_ERROR; /* non ? -> on s'arrête */
    }
    this->date = ligne;

    /* On enlève le '\n' */
    this->date.erase(this->date.length() - 1);

    /* 3eme ligne => grille */
    /* Est-ce qu'on peut bien lire une ligne ? */
    if ( !fgets(ligne, 200, file) )
    {
        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, tabLogMsg[READ_FILE_ERROR], filename);
        free(filename);
        return READ_FILE_ERROR; /* non ? -> on s'arrête */
    }
    /* On va chercher tous les tokens de la ligne */
    arrayToken = strtok(ligne, delimiter);

    /* Tant qu'on trouve un token, on continue */
    while (arrayToken != nullptr)
    {
        res = atoi(arrayToken);
        this->obstaclesArray[i] = res;

        /* Quand on trouve le robot on sauvegarde sa position */
        if (res == ROBOT)
            this->robotPosition = i;

        if (res == ARRIVEE)
            this->arriveePosition = i;

        if (res == PIEGE)
            this->listePiege.push_back(i);

        if (res == BOUE)
            this->listeBoue.push_back(i);

        /* On va chercher le prochain token s'il existe */
        arrayToken = strtok(nullptr, delimiter);
        i++;
    }

    /* On ferme le fichier */
    fclose(file);

    return 0;
}

int niveau::setRobotPosition(int pos)
{
    if (pos >= GRID_SIZE || pos < 0)
    {
        return OUT_OF_BOUND;
    }

    /* On enlève le robot de sa case */
    this->obstaclesArray[this->robotPosition] = RIEN;
    this->robotPosition = pos;
    this->obstaclesArray[this->robotPosition] = ROBOT;

    return 0;
}

int niveau::getRobotPosition()
{
    return this->robotPosition;
}

int niveau::getArriveePosition()
{
    return this->arriveePosition;
}

std::string niveau::getDate()
{
    return this->date;
}

std::string niveau::getCreator()
{
    return this->creator;
}

void niveau::setCreator(std::string creator_name)
{
    this->creator = creator_name;
}

void niveau::creerTableau(int i, int obstacle)
{
    this->obstaclesArray[i] = obstacle;
}

void niveau::reset()
{
    for (int i = 0; i < GRID_SIZE; i++)
        this->obstaclesArray[i] = 0;
}

void niveau::setRobotMort()
{
    this->robotMort = true;
}

bool niveau::getRobotMort()
{
    return this->robotMort;
}
