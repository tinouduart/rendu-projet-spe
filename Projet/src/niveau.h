#ifndef NIVEAU_H
#define NIVEAU_H

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <vector>

#ifdef __unix__
/* Pour pouvoir utiliser l'ORM */
#include <QxOrm.h>
#include "export.h"
#include <QxOrm_Impl.h>
#endif /* __unix__ */

#include "defs.h"

class niveau
{
public:

    long id;

    /* Position du robot */
    int robotPosition = 0;

    /* Position de l'arrivée */
    int arriveePosition = 0;

    /* Nom du créateur du niveau */
    std::string creator;

    /* Date de création */
    std::string date;

    bool robotMort = false;

    /* Tableau d'entier contenant les différents obstacles */
    int obstaclesArray[100];

    std::string obstaclesString;

    std::vector<int> listePiege;
    std::vector<int> listeBoue;

    /* constructeurs */
    niveau() : id(0) { ; }
    niveau(std::string creator_name);
    niveau(niveau* lvl);

    /* int chargerNiveau(char* filename);
     * Permet de charger un niveau en mémoire.
     *
     * char* filename : nom du fichier niveau.
     *
     * Renvoie 0 si tout s'est bien passé, un code d'erreur (< 0) sinon.
     */
    int chargerNiveau(char* filename);

    /* void setCreator(std::string creator_name);
     * Permet de définir le créateur d'un niveau.
     */
    void setCreator(std::string creator_name);

    /* std::string getDate();
     * Permet de récupérer la date de création du niveau.
     *
     * Renvoie une chaine de caractère contenant la date de création du niveau.
     */
    std::string getDate();

    /* std::string getCreator();
     * Permet de récupérer le créateur du niveau.
     *
     * Renvoie une chaine de caractère contenant le nom du créateur du niveau.
     */
    std::string getCreator();

    /* void creerTableau(int i, int obstacle);
     * Permet de créer le tableau d'obstacles.
     */
    void creerTableau(int i, int obstacle);

    /* int setRobotPosition(int pos);
     * Permet de définir la position du robot.
     *
     * Renvoie 0 si tout s'est bien passé, un code d'erreur (<0) sinon.
     */
    int setRobotPosition(int pos);

    /* int getRobotPosition();
     * Permet de récupérer la position actuelle du robot.
     *
     * Renvoie la position actuelle du robot.
     */
    int getRobotPosition();

    /* int getArriveePosition();
     * Permet de connaitre la position de la case d'arrivée.
     *
     * Renvoie la position de la case d'arrivée.
     */
    int getArriveePosition();

    void setRobotMort();

    bool getRobotMort();

    /* void reset();
     * Permet de réinitialiser la grille de jeu.
     */
    void reset();
};

#ifdef __unix__
QX_REGISTER_HPP_MY_TEST_EXE(niveau, qx::trait::no_base_class_defined, 1)
#endif /* __unix__ */
#endif // NIVEAU_H
