#ifndef PLAYER_H
#define PLAYER_H

#ifdef __unix__
#include <QxOrm.h>
#include <QxOrm_Impl.h>
#endif /* __unix__ */
#include "niveau.h"
#include <cstdint>
#include "defs.h"

class player
{
public:
    long id;

    std::string name;
    std::string password;
    int32_t lvlId;
    int32_t bestScore;

    /* Constructeurs */
    player() : id(0) { ; }
    player(std::string name, std::string password);
    player(player* p);
};

#ifdef __unix__
QX_REGISTER_HPP_MY_TEST_EXE(player, qx::trait::no_base_class_defined, 1)
#endif /* __unix__ */

#endif // PLAYER_H
