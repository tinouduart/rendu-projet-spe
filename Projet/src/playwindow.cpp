#include "playwindow.h"
#include "ui_playwindow.h"

#include <QtWidgets>
#include <QKeyEvent>

#include "defs.h"
#include "niveau.h"
#include "menu.h"
#include "log.h"

#include "externVars.h"

#ifdef __unix__
#include "ormapi.h"
#endif /* __unix__ */

static QVector<QPushButton*> buttonList(GRID_SIZE); /* Pareil qu'en haut => ça permettra de modifier les images des boutons */
static niveau lvl("");
QWidget* mainMenu = nullptr;
static int32_t nbCoups = 0;
bool boue = false;

/* playwindow::playwindow(QWidget *parent) :QMainWindow(parent),ui(new Ui::playwindow)
 * Création de la fenêtre et initialisation des variables globales;
 */
playwindow::playwindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::playwindow)
{
    ui->setupUi(this);
    qApp->installEventFilter(this);

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_CREATION_FENETRE]);
    const QSize size(BTN_WIDTH, BTN_HEIGHT);

    /* on veut 100 boutons */
    for(int i = 0; i < GRID_SIZE; i++)
    {
        /* création d'un bouton à chaque tour de boucle */
        buttonList[i] = new QPushButton("", this);

        /* On lui donne une taille fixe */
        buttonList[i]->setMinimumSize(BTN_WIDTH, BTN_HEIGHT);
        buttonList[i]->setIconSize(size);

        /* On le place dans le gridLayout à la colonne i/10 et à la ligne i%10 */
        ui->gridLayout->addWidget(buttonList.at(i), i / 10, i % 10);
    }
    ui->chargerButton->setFocusPolicy(Qt::NoFocus);

    if (parent)
        mainMenu = parent;
}

bool playwindow::eventFilter(QObject *obj, QEvent *event)
{
    /* Création des icones utilisées */
    QPixmap mur(MUR_PNG);
    QIcon icon_mur(mur);

    QPixmap robot(ROBOT_PNG);
    QIcon icon_robot(robot);

    QPixmap arrivee(ARRIVEE_PNG);
    QIcon icon_arrivee(arrivee);

    QPixmap gagne(GAGNE_PNG);
    QIcon icon_gagne(gagne);

    QPixmap mort(ROBOT_MORT_PNG);
    QIcon icon_mort(mort);

    QPixmap piege(PIEGE_PNG);
    QIcon icon_piege(piege);

    Q_UNUSED(obj);

    if (boue)
    {
        boue = false;
        return false;
    }

#ifdef __unix__
    ORMApi* db = NULL;

    /* Définitions de type pour faciliter la lecture du code */
    /*typedef std::shared_ptr<player> player_ptr;
    typedef std::vector<player_ptr> type_lst_player;*/

    QSqlError daoError;
#endif /* __unix__ */

    /* Touche enfoncée */
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *c = dynamic_cast<QKeyEvent *>(event);
        if(c && c->key() == Qt::Key_Up) /* Flèche du haut ? */
        {
            nbCoups++;
            for (int i = 0; i < MAX_INDEX_LINE; i++) /* Parcours d'une ligne */
            {
                /* Vérifications : le robot est mort ? On a gagné ? On est contre un mur ? */
                if (lvl.getRobotMort())
                {
                    return false;
                }

                if (lvl.getRobotPosition() == lvl.getArriveePosition())
                {
                    return false;
                }
                if (lvl.obstaclesArray[lvl.getRobotPosition() - 10] == MUR)
                {
                    return false;
                }
                /* On remonte d'un cran dans la grille */
                lvl.setRobotPosition(lvl.getRobotPosition() - 10);

                /* Parcourt du tableau d'obstacles */
                for (int i = 0; i < GRID_SIZE; i++)
                {
                    /* En fonction du tableau, on met à jour l'interface */
                    switch (lvl.obstaclesArray[i])
                    {
                        case RIEN:
                            buttonList[i]->setIcon(QIcon());
                            break;
                        case MUR:
                            buttonList[i]->setIcon(icon_mur);
                            break;
                        case ROBOT:
                            buttonList[i]->setIcon(icon_robot);
                            break;
                        case ARRIVEE:
                            buttonList[i]->setIcon(icon_arrivee);
                            break;
                        case PIEGE:
                            buttonList[i]->setIcon(icon_piege);
                            break;
                        default:
                            break;
                    }
                }
                if (lvl.getRobotPosition() == lvl.getArriveePosition()) /* On vérifie si l'arrivée a été atteinte */
                {   /* On a atteint la case d'arrivée */
                    lvl.setRobotPosition(lvl.getRobotPosition());
                    buttonList[lvl.getRobotPosition()]->setIcon(gagne);
                    QMessageBox::information(this, tr("GAGNE"), "t'es trop fort", QMessageBox::Ok);

                    player1->bestScore = CALC_BEST_SCORE(nbCoups);
                    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_PLAYER_SCORE], player1->name.c_str(), player1->bestScore);
                    nbCoups = 0; /* RAZ nbCoups */
                    break;
                }
                for (auto i = lvl.listePiege.begin(); i != lvl.listePiege.end(); i++)
                { /* Parcours de la liste des index des pièges */
                    if (lvl.getRobotPosition() == *i)
                    {   /* On tombe sur un piège : on s'arrête */
                        lvl.setRobotMort();
                        buttonList[lvl.getRobotPosition()]->setIcon(mort);
                        QMessageBox::critical(this, tr("PERDU"), "tu es mort", QMessageBox::Ok);
                        return false;
                    }
                }
                for (auto i = lvl.listeBoue.begin(); i != lvl.listeBoue.end(); i++)
                { /* Parcours de la liste des index des pièges */
                    if (lvl.getRobotPosition() == *i)
                    {   /* On tombe sur un piège : on s'arrête */
                        boue = true;
                        break;
                    }
                }
                if (boue)
                    return false;
            }
        }

        if (c && c->key() == Qt::Key_Down) /* Flèchp1e du bas */
        {
            nbCoups++;
            for (int i = 0; i < MAX_INDEX_LINE; i++)
            {
                /* Vérifications */
                if (lvl.getRobotMort())
                {
                    return false;
                }

                if (lvl.getRobotPosition() == lvl.getArriveePosition())
                {
                    return false;
                }

                if (lvl.obstaclesArray[lvl.getRobotPosition() + 10] == MUR)
                {
                    return false;
                }
                /* On descend d'une case */
                lvl.setRobotPosition(lvl.getRobotPosition() + 10);

                /* Parcourt du tableau d'obstacles */
                for (int i = 0; i < GRID_SIZE; i++)
                {
                    /* En fonction du tableau, on met à jour l'interface */
                    switch (lvl.obstaclesArray[i])
                    {
                        case RIEN:
                            buttonList[i]->setIcon(QIcon());
                            break;
                        case MUR:
                            buttonList[i]->setIcon(icon_mur);
                            break;
                        case ROBOT:
                            buttonList[i]->setIcon(icon_robot);
                            break;
                        case ARRIVEE:
                            buttonList[i]->setIcon(icon_arrivee);
                            break;
                        case PIEGE:
                            buttonList[i]->setIcon(icon_piege);
                            break;
                        default:
                            break;
                    }
                }
                if (lvl.getRobotPosition() == lvl.getArriveePosition())
                {
                    lvl.setRobotPosition(lvl.getRobotPosition());
                    buttonList[lvl.getRobotPosition()]->setIcon(gagne);
                    QMessageBox::information(this, tr("GAGNE"), "t'es trop fort", QMessageBox::Ok);

                    player1->bestScore = CALC_BEST_SCORE(nbCoups);
                    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_PLAYER_SCORE], player1->name.c_str(), player1->bestScore);
                    nbCoups = 0; /* RAZ nbCoups */
#ifdef __unix__
                    db = new ORMApi((char*)"QSQLITE", (char*)"test_qxorm.db", (char*)"localhost", (char*)"root", (char*)"");
                    qx::dao::update(player1);
#endif /* __unix__ */
                    break;
                }

                for (auto i = lvl.listePiege.begin(); i != lvl.listePiege.end(); i++)
                {
                    if (lvl.getRobotPosition() == *i)
                    {
                        lvl.setRobotMort();
                        buttonList[lvl.getRobotPosition()]->setIcon(mort);
                        QMessageBox::critical(this, tr("PERDU"), "tu es mort", QMessageBox::Ok);
                        return false;
                    }
                }
                for (auto i = lvl.listeBoue.begin(); i != lvl.listeBoue.end(); i++)
                { /* Parcours de la liste des index des pièges */
                    if (lvl.getRobotPosition() == *i)
                    {   /* On tombe sur un piège : on s'arrête */
                        boue = true;
                        break;
                    }
                }
                if (boue)
                    break;
            }
        }

        if (c && c->key() == Qt::Key_Left) /* Flèche de gauche */
        {
            nbCoups++;
            for (int i = 0; i < MAX_INDEX_LINE; i++)
            {
                if (lvl.getRobotMort())
                {
                    return false;
                }

                if (lvl.getRobotPosition() == lvl.getArriveePosition())
                {
                    return false;
                }

                /* On descend d'un cran dans la grille */
                if (lvl.getRobotPosition() % LINE_LENGTH == MIN_INDEX_LINE)
                {
                    return false;
                }

                if (lvl.obstaclesArray[lvl.getRobotPosition() - 1] == MUR)
                {
                    return false;
                }

                /* On déplace le robot à gauche sur la ligne */
                lvl.setRobotPosition(lvl.getRobotPosition() - 1);

                /* Parcourt du tableau d'obstacles */
                for (int i = 0; i < GRID_SIZE; i++)
                {
                    /* En fonction du tableau, on met à jour l'interface */
                    switch (lvl.obstaclesArray[i])
                    {
                        case RIEN:
                            buttonList[i]->setIcon(QIcon());
                            break;
                        case MUR:
                            buttonList[i]->setIcon(icon_mur);
                            break;
                        case ROBOT:
                            buttonList[i]->setIcon(icon_robot);
                            break;
                        case ARRIVEE:
                            buttonList[i]->setIcon(icon_arrivee);
                            break;
                        case PIEGE:
                            buttonList[i]->setIcon(icon_piege);
                            break;
                        default:
                            break;
                    }
                }
                if (lvl.getRobotPosition() == lvl.getArriveePosition())
                {
                    lvl.setRobotPosition(lvl.getRobotPosition());
                    buttonList[lvl.getRobotPosition()]->setIcon(gagne);
                    QMessageBox::information(this, tr("GAGNE"), "t'es trop fort", QMessageBox::Ok);

                    player1->bestScore = CALC_BEST_SCORE(nbCoups);
                    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_PLAYER_SCORE], player1->name.c_str(), player1->bestScore);
                    nbCoups = 0; /* RAZ nbCoups */
                    break;
                }
                for (auto i = lvl.listePiege.begin(); i != lvl.listePiege.end(); i++)
                {
                    if (lvl.getRobotPosition() == *i)
                    {
                        lvl.setRobotMort();
                        buttonList[lvl.getRobotPosition()]->setIcon(mort);
                        QMessageBox::critical(this, tr("PERDU"), "tu es mort", QMessageBox::Ok);
                        return false;
                    }
                }
                for (auto i = lvl.listeBoue.begin(); i != lvl.listeBoue.end(); i++)
                { /* Parcours de la liste des index des pièges */
                    if (lvl.getRobotPosition() == *i)
                    {   /* On tombe sur un piège : on s'arrête */
                        boue = true;
                        break;
                    }
                }
                if (boue)
                    break;
            }
        }

        if (c && c->key() == Qt::Key_Right) /* Flèche de droite */
        {
            nbCoups++;
            for (int i = 0; i < MAX_INDEX_LINE; i++)
            {
                /* Vérifications */
                if (lvl.getRobotMort())
                {
                    return false;
                }

                if (lvl.getRobotPosition() == lvl.getArriveePosition())
                {
                    return false;
                }

                if (lvl.getRobotPosition() % LINE_LENGTH == MAX_INDEX_LINE)
                {
                    return false;
                }

                if (lvl.obstaclesArray[lvl.getRobotPosition() + 1] == MUR)
                {
                    return false;
                }

                /* On déplace le robot à droite sur la ligne */
                lvl.setRobotPosition(lvl.getRobotPosition() + 1);

                /* Parcourt du tableau d'obstacles */
                for (int i = 0; i < GRID_SIZE; i++)
                {
                    /* En fonction du tableau, on met à jour l'interface */
                    switch (lvl.obstaclesArray[i])
                    {
                        case RIEN:
                            buttonList[i]->setIcon(QIcon());
                            break;
                        case MUR:
                            buttonList[i]->setIcon(icon_mur);
                            break;
                        case ROBOT:
                            buttonList[i]->setIcon(icon_robot);
                            break;
                        case ARRIVEE:
                            buttonList[i]->setIcon(icon_arrivee);
                            break;
                        case PIEGE:
                            buttonList[i]->setIcon(icon_piege);
                            break;
                        default:
                            break;
                    }
                }
                if (lvl.getRobotPosition() == lvl.getArriveePosition())
                {
                    lvl.setRobotPosition(lvl.getRobotPosition());
                    buttonList[lvl.getRobotPosition()]->setIcon(gagne);
                    QMessageBox::information(this, tr("GAGNE"), "t'es trop fort", QMessageBox::Ok);

                    player1->bestScore = CALC_BEST_SCORE(nbCoups);
                    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_PLAYER_SCORE], player1->name.c_str(), player1->bestScore);
                    nbCoups = 0; /* RAZ nbCoups */
                    break;
                }
                for (auto i = lvl.listePiege.begin(); i != lvl.listePiege.end(); i++)
                {
                    if (lvl.getRobotPosition() == *i)
                    {
                        buttonList[lvl.getRobotPosition()]->setIcon(mort);
                        lvl.setRobotMort();
                        QMessageBox::critical(this, tr("PERDU"), "tu es mort", QMessageBox::Ok);
                        return false;
                    }
                }
                for (auto i = lvl.listeBoue.begin(); i != lvl.listeBoue.end(); i++)
                { /* Parcours de la liste des index des pièges */
                    if (lvl.getRobotPosition() == *i)
                    {   /* On tombe sur un piège : on s'arrête */
                        boue = true;
                        break;
                    }
                }
                if (boue)
                    break;
            }
        }

        if(c && c->key() == Qt::Key_Escape)
        {
            QCoreApplication::exit(0);
        }
    }
    return false;
}

int playwindow::charger()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Load Level"), ".", tr("Level Files (*.lvl)"));

    std::string tmp;
    QString temp;

    QPixmap mur(MUR_PNG);
    QIcon icon_mur(mur);

    QPixmap robot(ROBOT_PNG);
    QIcon icon_robot(robot);

    QPixmap arrivee(ARRIVEE_PNG);
    QIcon icon_arrivee(arrivee);

    QPixmap piege(PIEGE_PNG);
    QIcon icon_piege(piege);

    QPixmap boue(BOUE_PNG);
    QIcon icon_boue(boue);

    /* Initialisation d'un message d'erreur (au cas où) */
    QString err = "";

    /* char* (chaine de caractère au format du langage C : contiendra le nom du fichier à charger) */
    char* c_file_name = nullptr;

    /* Contiendra la longueur de la chaine de caractère : mieux vaut ne la calculer qu'une fois plutôt que faire plusieurs appel à la méthode size() */
    int32_t filename_length = 0;

    /* return code */
    int rc = 0;

    /* On récupère la taille de la chaine filename */
    filename_length = (int32_t)filename.toStdString().size();

    /* Allocation mémoire pour la chaine */
    c_file_name = reinterpret_cast<char*>(malloc((filename_length + 1) * sizeof(char)));
    if ( !c_file_name )
    {
        /* On crée le message d'erreur */
        err.append(QString("Error ")).append(QString::number(MEMORY_ALLOCATION_ERR)).append(" : Not enough memory.");

        /* On l'affiche dans une messageBox */
        QMessageBox::critical(this, tr("Error"), err, QMessageBox::Cancel);
        logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_ERROR, tabLogMsg[MEMORY_ALLOCATION_ERR]);
        return MEMORY_ALLOCATION_ERR;
    }

    /* Copie de la chaine filename format C++ vers un char* (format C) */
    filename.toStdString().copy(c_file_name, filename_length + 1);

    /* On place le caractère null `\0` à la fin pour indiquer la fin de la chaine */
    c_file_name[filename.toStdString().size()] = '\0';

    /* Création d'un objet niveau pour pouvoir charger un niveau en mémoire */
    lvl = niveau("");
    rc = lvl.chargerNiveau(c_file_name);

    /* Gestion des erreurs */
    switch (rc)
    {
        case 0:
            break;
        case FILE_NOT_OPENED:
            /* On crée le message d'erreur */
            err.append(QString("Error ")).append(QString::number(FILE_NOT_OPENED)).append(" : Cannot open file.");

            /* On l'affiche dans une messageBox */
            QMessageBox::critical(this, tr("Error"), err, QMessageBox::Cancel);
            return FILE_NOT_OPENED;
        case READ_FILE_ERROR:
            /* On crée le message d'erreur */
            err.append(QString("Error ")).append(QString::number(READ_FILE_ERROR)).append(" : Cannot read file.");

            /* On l'affiche dans une messageBox */
            QMessageBox::critical(this, tr("Error"), err, QMessageBox::Cancel);
            return READ_FILE_ERROR;
    }

    /* Parcourt du tableau d'obstacles */
    for (int i = 0; i < GRID_SIZE; i++)
    {
        /* En fonction du tableau, on met à jour l'interface */
        switch (lvl.obstaclesArray[i])
        {
            case RIEN:
                buttonList[i]->setIcon(QIcon());
                break;
            case MUR:
                buttonList[i]->setIcon(icon_mur);
                break;
            case ROBOT:
                buttonList[i]->setIcon(icon_robot);
                break;
            case ARRIVEE:
                buttonList[i]->setIcon(icon_arrivee);
                break;
            case PIEGE:
                buttonList[i]->setIcon(icon_piege);
                break;
            case BOUE:
                buttonList[i]->setIcon(icon_boue);
                break;
            default:
                break;
        }
    }

    /* On libère la mémoire allouée */
    free(c_file_name);

    ui->creatorLabel->setText("Creator : ");
    tmp = lvl.getCreator();
    temp = QString::fromStdString(tmp);
    ui->creatorLabel->setText(ui->creatorLabel->text() + temp);

    ui->dateLabel->setText("Date : ");
    tmp = lvl.getDate();
    temp = QString::fromStdString(tmp);
    ui->dateLabel->setText(ui->dateLabel->text() + temp);

    logger(LOG_SYS_FILE, __FUNCTION__, LOG_LEVEL_INFO, tabLogMsg[LOGMSG_LOAD_LEVEL_SUCCEED]);
    /* Tout s'est bien passé, on renvoie 0 */
    return 0;
}

playwindow::~playwindow()
{
    mainMenu->show();
    /* Libération de la mémoire pour tous les boutons */
    for (int i = 0; i < GRID_SIZE; i++)
        delete buttonList[i];

    delete ui;
}

void playwindow::on_chargerButton_clicked()
{
    charger();
}
