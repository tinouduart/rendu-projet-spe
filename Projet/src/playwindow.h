#ifndef PLAYWINDOW_H
#define PLAYWINDOW_H

#include <QMainWindow>
#include "player.h"

namespace Ui {
class playwindow;
}

class playwindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit playwindow(QWidget *parent = nullptr);
    ~playwindow();

    /* Fonction de récupération des évènements clavier (appuis sur les touches).
     * Touches prises en charge : touches fléchées, ESCAPE.
     */
    bool eventFilter(QObject *obj, QEvent *event);

private slots:
    /* void on_chargerButton_clicked();
     * Appelle la fonction `charger()`
     */
    void on_chargerButton_clicked();
private:
    Ui::playwindow *ui;

    /* int charger();
     * Permet de charger un niveau.
     *
     * Renvoie 0 si tout s'est bien passé, un code d'erreur (<0) sinon.
     */
    int charger();
};

#endif // PLAYWINDOW_H
