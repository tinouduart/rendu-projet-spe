#include "solveur.h"
#ifndef _WIN32
#include <bits/stdc++.h>
#endif

solveur::solveur(int8_t nbVertices)
{
    this->numVertices = nbVertices;
    this->adjMatrix = new int8_t*[nbVertices];
    for (int i = 0; i < nbVertices; i++)
    {
        this->adjMatrix[i] = new int8_t[nbVertices];
        for (int j = 0; j < nbVertices; j++)
            this->adjMatrix[i][j] = 0;
    }
}

void solveur::addEdge(int8_t i, int8_t j)
{
    this->adjMatrix[i][j] = 1;
    this->adjMatrix[j][i] = 1;
}

void solveur::removeEdge(int8_t i, int8_t j)
{
    this->adjMatrix[i][j] = 0;
    this->adjMatrix[j][i] = 0;
}

bool solveur::isEdge(int8_t i, int8_t j)
{
    return this->adjMatrix[i][j];
}

void solveur::toString()
{
    for (int i = 0; i < this->numVertices; i++)
    {
        std::cout << i << " : \t";
        for (int j = 0; j < this->numVertices; j++)
            std::cout << this->adjMatrix[i][j] << " ";
        std::cout << "\n";
      }
}

// A utility function to find the vertex with minimum distance value, from
// the set of vertices not yet included in shortest path tree
int minDistance(int dist[], bool sptSet[])
{
    // Initialize min value
    int min = INT_MAX, min_index;

    for (int v = 0; v < 16; v++)
        if (sptSet[v] == false && dist[v] <= min)
            min = dist[v], min_index = v;

    return min_index;
}

// A utility function to print the constructed distance array
void printSolution(int dist[])
{
    printf("Vertex \t\t Distance from Source\n");
    for (int i = 0; i < 16; i++)
        printf("%d \t\t %d\n", i, dist[i]);
}

// Function that implements Dijkstra's single source shortest path algorithm
// for a graph represented using adjacency matrix representation
int16_t solveur::dijkstra(int8_t** graph, int8_t src, int16_t arrivee)
{
    int dist[16]; // The output array.  dist[i] will hold the shortest
    // distance from src to i

    bool sptSet[16]; // sptSet[i] will be true if vertex i is included in shortest
    // path tree or shortest distance from src to i is finalized

    // Initialize all distances as INFINITE and stpSet[] as false
    for (int i = 0; i < 16; i++)
        dist[i] = INT_MAX, sptSet[i] = false;

    // Distance of source vertex from itself is always 0
    dist[src] = 0;

    // Find shortest path for all vertices
    for (int count = 0; count < 16 - 1; count++) {
        // Pick the minimum distance vertex from the set of vertices not
        // yet processed. u is always equal to src in the first iteration.
        int u = minDistance(dist, sptSet);

        // Mark the picked vertex as processed
        sptSet[u] = true;

        // Update dist value of the adjacent vertices of the picked vertex.
        for (int v = 0; v < 16; v++)

            // Update dist[v] only if is not in sptSet, there is an edge from
            // u to v, and total weight of path from src to  v through u is
            // smaller than current value of dist[v]
            if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
                && dist[u] + graph[u][v] < dist[v])
                dist[v] = dist[u] + graph[u][v];
    }

    if (dist[arrivee] > 0)
        return true;
    else
        return false;
}
