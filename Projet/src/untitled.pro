include(/home/tinouduart/Téléchargements/QxOrm/QxOrm.pri)

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
win32{
DEFINES += _WIN32
DEFINES += _CRT_SECURE_NO_WARNINGS
}

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ./logSystem/include
INCLUDEPATH += ./include

LIBS += dl -lboost_serialization -L/home/tinouduart/Téléchargements/QxOrm/lib/ -lQxOrm -L/home/tinouduart/Documents/Programmation/C++/QT_ORM_API/build-ORMApi-Desktop_Qt_5_14_1_GCC_64bit-Debug/ -lORMApi -L/home/tinouduart/Documents/Programmation/C/systeme_de_log/ -llogc

SOURCES += \
    dialog_solveur.cpp \
    main.cpp \
    mainwindow.cpp \
    menu.cpp \
    niveau.cpp \
    player.cpp \
    playwindow.cpp \
    solveur.cpp

HEADERS += \
    defs.h \
    dialog_solveur.h \
    export.h \
    externVars.h \
    mainwindow.h \
    menu.h \
    niveau.h \
    player.h \
    playwindow.h \
    solveur.h

FORMS += \
    dialog_solveur.ui \
    mainwindow.ui \
    menu.ui \
    playwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
