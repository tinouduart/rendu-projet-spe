# Robot Ricochet

## Règle du jeu :
C'est un labyrinthe. La seul différence est que l'on ne peut tourner QUE lorsque l'on rencontre un obstacle.
Le joueur gagne lorsqu'il arrive sur la case d'arrivée. Il perd quand il tombe sur une case piège.

## Pré-requis
Sous windows et linux: 
- Qt5.14.1.

Sous linux:
- CMake
- libboost

## Compiler le programme
Si vous avez besoin de compiler le programme robot_ricochet, plusieurs solutions s'offrent à vous :
- Ouvir le fichier .pro présent dans le répertoire source et ensuite compiler depuis Qt,
- Sous linux, créer un répertoire bin dans le répertoire de source et lancer la commande ```cmake .. && make```

S'il faut recompiler l'ORM, les sources sont disponibles sur le site officiel : [QxOrm](https://www.qxorm.com/qxorm_fr/home.html)

## Lancer le programme
Il suffit de double cliquer dessus.
Une fenêtre de connexion apparaitra et il vous sera demander un identifiant ainsi qu'un mot de passe. Par défaut il s'agit de 'root' et 'pass'

Vous pouvez jouer !